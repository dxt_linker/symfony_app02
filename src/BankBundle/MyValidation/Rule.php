<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 19.09.16
 * Time: 03:53
 */

namespace BankBundle\MyValidation;


class Rule
{
    public static function not_empty_string( $value ) 
    {
        return (bool) (strlen($value) > 0) ;
    }
    
    public static function cnp_code( $value )
    {
        return preg_match("/^\d{3}-\d{2}-\d{4}$/", $value, $output_array);
    }
    
    public static function numeric( $value )
    {
        return is_numeric($value);
    }


    public static function datetime( $value, $format )
    {
        $d = \DateTime::createFromFormat($format, $value);
        return $d AND $d->format($format) == $value;

    }

}