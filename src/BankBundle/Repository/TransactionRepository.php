<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 19.09.16
 * Time: 11:20
 */

namespace BankBundle\Repository;

use Doctrine\DBAL\Query\QueryBuilder;
use \Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class TransactionRepository extends EntityRepository
{

    protected $convert = [
        'eq'    => '=',
        'lt'    => '<',
        'gt'    => '>'
    ];

    /**
     * For query string validation
     *
     * @param \Symfony\Component\HttpFoundation\ParameterBag $query
     * @return array
     */
    public function _query_to_context(\Symfony\Component\HttpFoundation\ParameterBag $query)
    {

        $context = [

            'limit'             => (int) $query->get('c_limit',               12),
            'offset'            => (int) $query->get('c_offset',              0),
            'amount'            => (int) $query->get('c_amount',              ''),
            'date'              => $query->get('c_date',                      ''),
        ];

        $filter = [];

        # amount WHERE
        if(!empty($context['amount'])){

            $f_amount   = $query->get('f_amount',   'eq');

            if(!in_array($f_amount,    ['eq','lt','gt']))
                throw new UnprocessableEntityHttpException();

            $filter['amount']       = [ $this->convert[$f_amount], $context['amount'] ];
        }

        # date WHERE
        if(!empty($context['date'])){

            # format d.m.Y

            $f_date     = $query->get('f_date',     'eq');

            if(!in_array($f_date,       ['eq','lt','gt']))
                throw new UnprocessableEntityHttpException();

            # date conversations;
            $date = \DateTime::createFromFormat('d.m.Y H:i:s', $context['date'] . ' 00:00:00');
            $mysql_date = $date->format('Y-m-d H:i:s');

            $filter['createdAt']         = [ $this->convert[$f_date], $mysql_date ];
        }

        if(!empty( $context['customerID'] ))
            $filter['customerID']   = [ $this->convert['eq'],   $context['customerID'] ];

        if(!empty( $context['transactionID'] ))
            $filter['transactionID']= [ $this->convert['eq'],   $context['transactionID'] ];


        return [

            'filter'    => $filter,
            'context'   => $context,

        ];

    }

    protected function _apply_context_filter($alias, array $filter, \Doctrine\ORM\QueryBuilder $query)
    {

        $i = 0;

        foreach ($filter as $column => $search )
        {

            list($compare, $value) = $search;


            if($i == 0){
                $query
                    ->where("{$alias}.{$column} {$compare} :val{$column}")
                    ->setParameter("val{$column}", $value);
            }else{
                $query
                    ->andWhere("{$alias}.{$column} {$compare} :val{$column}")
                    ->setParameter("val{$column}", $value);
            }

            $i++;

        }

    }


    public function findAndFiltrate_COUNT( array $filter )
    {

        $query = $this
            ->createQueryBuilder('c')
            ->select('count(c)');

        $this->_apply_context_filter('c', $filter['filter'], $query);

        $query = $query->getQuery()->getOneOrNullResult();

        return (int) $query[1];
        
        
    }

    public function findAndFiltrate_DATA( array $filter, $as_array = false)
    {

        $query = $this
            ->createQueryBuilder('d')
            ->select('d');

        $context = $this->_apply_context_filter('d', $filter['filter'], $query );

        $query = $query
            ->setMaxResults($context['context']['limit'])
            ->setFirstResult($context['context']['offset'])
            ->orderBy('d.id','DESC');

        return $query->getQuery()->getResult(

            $as_array
                ? \Doctrine\ORM\Query::HYDRATE_ARRAY
                : \Doctrine\ORM\Query::HYDRATE_OBJECT

        );

    }

}