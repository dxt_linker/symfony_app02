<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 19.09.16
 * Time: 10:34
 */

namespace BankBundle\Entity;


use BankBundle\Entity\Traits\Validator;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BankBundle\Repository\TransactionRepository")
 * @ORM\Table(name="transaction")
 */
class Transaction
{
    
    use Validator;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="BankBundle\Entity\Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;
    /**
     * @ORM\Column(type="decimal", precision=19, scale=2)
     */
    private $amount;
    /**
     * @ORM\Column(type="datetime", )
     */
    private $createdAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer(Customer $customerID)
    {
        $this->customer = $customerID;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        
        $this->validation_constraint($amount, 'amount', ['numeric']);
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt, $format = 'd.m.Y H:i:s')
    {
        $value = \DateTime::createFromFormat($format, $createdAt);
        $value = ($value) ? $value->format('Y-m-d H:i:s') : $value;

        if($value)
            $this->createdAt = $value;

        $this->validation_constraint($createdAt, 'createdAt', [ ['datetime', 'd.m.Y H:i:s'] ]);
    }

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

}