<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 19.09.16
 * Time: 14:54
 */

namespace BankBundle\Entity\Traits;


trait Validator
{

    protected $_validate = [];
    /**
     * As Validator Constraints don't want to work - i use own manual validation
     * TODO:: check what's wrong with default validator
     */
    public function validation_failed()
    {

        $validation = [];

        foreach ($this->_validate as $column => $validator)
        {

            list($value, $rules) = $validator;

            foreach ($rules as $rule)
            {

                if( is_array($rule) )
                {

                    $arguments = $rule;
                    $rule = array_shift($arguments);
                    array_unshift($arguments, $value);

                }
                else
                {

                    $arguments = [$value];

                }

                if( !call_user_func_array("BankBundle\MyValidation\Rule::{$rule}", $arguments) ){ // not passed validation

                    if(!array_key_exists($column, $validation))
                        $validation[ $column ] = [];

                    $validation[ $column ][] = $rule.':'.implode(';',$arguments); // fill list of not passed rules

                }

            }

        }

        return $validation;

    }

    protected function validation_constraint($value, $columnName, array $rules){

        $this->_validate[$columnName] = [$value, $rules];


    }

    
}