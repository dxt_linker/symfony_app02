<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 19.09.16
 * Time: 02:27
 */

namespace BankBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
//use Symfony\Component\Validator\Tests\Constraints as Assert;  # don't want to autoload.. will validate manually
/**
 * @ORM\Entity(repositoryClass="BankBundle\Repository\CustomerRepository")
 * @ORM\Table(name="customer")
 * @UniqueEntity("cnp")
 */
class Customer
{
    
    use Traits\Validator;
    
    const API_ERR_CNP_CODE_INSERT_DUPLICATE = 'cpn:duplicate';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     * //@Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="cnp", type="string", length=11, unique=true)
     *
     */
    private $cnp;

    /**
     * @ORM\OneToMany(targetEntity="BankBundle\Entity\Transaction", mappedBy="customer")
     */
    private $transaction;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = trim($name);

        $this->validation_constraint($this->name, 'name',['not_empty_string']);
    }

    /**
     * @return mixed
     */
    public function getCnp()
    {
        return $this->cnp;
    }

    /**
     * @param mixed $cnp
     */
    public function setCnp($cnp)
    {

        $this->cnp = trim($cnp);

        $this->validation_constraint($this->cnp, 'cnp',['cnp_code']);

    }

}