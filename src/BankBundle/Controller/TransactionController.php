<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 19.09.16
 * Time: 10:59
 */

namespace BankBundle\Controller;



use BankBundle\Entity\Customer;
use BankBundle\Entity\Transaction;
use BankBundle\MyApi\Instance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TransactionController extends Controller
{
    const NOT_EXISTS = 404;
    
    public function loadOneAction($customerID, $transactionID)
    {


        if( Request::createFromGlobals()->isMethod('POST') )
            return $this->updateTransaction($customerID, $transactionID);

        if( Request::createFromGlobals()->isMethod('DELETE') )
            return $this->deleteTransactionAction($customerID, $transactionID);


        $em     = $this->getDoctrine()->getManager();
        $repo   = $em->getRepository('BankBundle:Transaction');
        $api    = new Instance();

        $result = $repo->findOneBy([
            # ORM field     => $value
            'customer'      => $customerID,
            'id'            => $transactionID,

        ]);

        $state  = [
            'read'  => 0,
        ];

        if(is_null($result))
        {

            $api->msg_addError( $state['read'], self::NOT_EXISTS );

        }
        else
        {

            $api->data['amount']        = $result->getAmount();
            $api->data['created']       = $result->getCreatedAt();
            $api->data['customerID']    = $result->getCustomer()->getId();
            $api->data['transactionID'] = $result->getId();

        }

        return $api->toResponse();
        
    }
    
    public function loadListAction()
    {

        $query = Request::createFromGlobals()->query;

        $em     = $this->getDoctrine()->getManager();
        $repo   = $em->getRepository('BankBundle:Transaction');
        $api    = new Instance();

        $api->data['context']   = $repo->_query_to_context( $query );
        $api->data['count']     = $repo->findAndFiltrate_COUNT( $api->data['context'] );
        $api->data['results']   = $repo->findAndFiltrate_DATA(  $api->data['context'], true );

        return $api->toResponse();
        
    }

    public function createAction($customerID)
    {
        #begin
        $em = $this->getDoctrine()->getManager();
        $transaction = new Transaction();
        $api = new Instance();
        $state = [

            'validation'    => 0,
            'not_exists'    => 1,

        ];

        #read
        $post = Request::createFromGlobals()->request;;

        $customer_repo = $em->getRepository('BankBundle:Customer');
        $customer = $customer_repo->find($customerID);

        ## |error response
        if(is_null($customer_repo)){

            $api->msg_addError($state['not_exists'],self::NOT_EXISTS);

            return $api->toResponse();

        }

        #set
        $transaction->setAmount($post->get('amount','-'));
        $transaction->setCustomer($customer);

        #validate
        if($messages = $transaction->validation_failed())
        {

            foreach ($messages as $column => $rules)
            {
                foreach ($rules as $rule)
                {

                    $api->msg_addError($state['validation'], "{$column}:{$rule}");

                }
            }

        }

        ## |error response
        if(!$api->getStatus($state['validation']))
            return $api->toResponse();

        #save
        $em->persist( $transaction );
        $em->flush();

        $api->data['transaction'] = [

            'transactionID' => $transaction->getId(),
            'customerID'    => $customerID,
            'amount'        => $transaction->getAmount(),
            'date'          => $transaction->getCreatedAt(),

        ];

        return $api->toResponse();

    }

    public function updateTransaction($customerID, $transactionID)
    {

        #begin
        $em     = $this->getDoctrine()->getManager();
        $repo   = $em->getRepository('BankBundle:Transaction');
        $api    = new Instance();
        $post   = Request::createFromGlobals()->request;

        $state = [
            'not_found'     => 0,
            'validation'    => 1,
            'executed'      => 2,
        ];

        #read
        $transaction = $repo->findOneBy( [

                'id'                => $transactionID,
                'customer'          => $customerID,

        ]);


        #check if loaded
        if( is_null($transaction) )
        {

            $api->msg_addError($state['not_found'],self::NOT_EXISTS);

            return $api->toResponse();

        }

        #set
        $amount = $post->get('amount','');
        $date   = $post->get('date','');

        if($amount !== '')
            $transaction->setAmount( $amount );

        if($date !== '')
            $transaction->setCreatedAt($date);

        #validate
        if($messages = $transaction->validation_failed())
        {

            foreach ($messages as $column => $rules)
            {
                foreach ($rules as $rule)
                {

                    $api->msg_addError($state['validation'], "{$column}:{$rule}");

                }
            }

        }

        ## |error response
        if(!$api->getStatus($state['validation']))
            return $api->toResponse();

        ### IT GIVES ME ERROR on date save, whatever I tried
        //TODO :: FIX
        //$em->refresh( $transaction );
        //$em->clear();
        //$em->flush();

        $api->msg_addSuccess($state['executed'], 'success, but...');

        return $api->toResponse();

    }

    public function deleteTransactionAction($customerID, $transactionID){

        #begin
        $em     = $this->getDoctrine()->getManager();
        $repo   = $em->getRepository('BankBundle:Transaction');
        $api    = new Instance();

        $state  = [
            'read'      => 0,
            'executon'  => 1,
        ];

        #read
        $result = $repo->findOneBy([
            # ORM field     => $value
            'customer'      => $customerID,
            'id'            => $transactionID,

        ]);

        if(is_null($result))
        {

            $api->msg_addError( $state['read'], self::NOT_EXISTS );

            return $api->toResponse();

        }

        $em->remove( $result );
        $em->flush();

        $api->msg_addSuccess($state['executon'], 'deleted');

        return $api->toResponse();

    }

}