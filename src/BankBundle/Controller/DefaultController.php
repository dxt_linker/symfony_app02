<?php

namespace BankBundle\Controller;

use BankBundle\Entity\Customer;
use BankBundle\MyApi\Instance;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function newCustomerAction()
    {
        #begin
        $customer = new Customer();
        $api = new Instance();

        $state = [

            'validation'    => 0,
            'duplicate'     => 1,

        ];

        #read
        $post = Request::createFromGlobals()->request;;

        #set
        $customer->setName( $post->get('name')  );
        $customer->setCnp(  $post->get('cnp')   );
        
        #validate
        if($messages = $customer->validation_failed())
        {

            foreach ($messages as $column => $rules)
            {
                foreach ($rules as $rule)
                {
                    
                    $api->msg_addError($state['validation'], "{$column}:{$rule}");
                    
                }
            }
                
        }

        ## |error response
        if(!$api->getStatus($state['validation']))
            return $api->toResponse();

        #save
        $em = $this->getDoctrine()->getManager();
        $em->persist( $customer );

        try {

            $em->flush();
            $api->data['customerID'] = $customer->getId();

        }
        catch (UniqueConstraintViolationException $e){

            $api->msg_addError($state['duplicate'],Customer::API_ERR_CNP_CODE_INSERT_DUPLICATE);

        }

        ## |final response
        return $api->toResponse();

    }
}
