<?php
/**
 * Created by PhpStorm.
 * User: LINKeR
 * Date: 19.09.16
 * Time: 04:19
 */

namespace BankBundle\MyApi;


use Symfony\Component\HttpFoundation\JsonResponse;

class Instance
{
    const MSG_SUCCESS_KEY   = 0;
    const MSG_ERROR_KEY     = 1;

    const STATUS_OK         = 2;
    const STATUS_ERRROR     = 3;
    const STATUS_WARNING    = 4;
    
    private $_data  = [
        'd' => [],     # data
        'm' => [],     # message
    ];

    public $data    = [];

    public function __construct()
    {
        $this->_data['d'] =& $this->data;
    }

    private function _add_msg($key, $token, $message){

        # create blank data structure for handling status;
        if(!array_key_exists($token, $this->_data['m']))
            $this->_data['m'][$token] = [

                self::MSG_ERROR_KEY     => [],
                self::MSG_SUCCESS_KEY   => [],

            ];

        # add a message for a token, by type
        $this->_data['m'][$token][$key][] = $message;

    }

    /**
     * Add success message
     * @param $token string     name for group of messages that defines some state
     * @param $message string   message to show via API
     */
    public function msg_addSuccess($token, $message)
    {
        $this->_add_msg(self::MSG_SUCCESS_KEY, $token, $message);
    }

    /**
     * Add error message
     * @param $token string     name for group of messages that defines some state
     * @param $message string   message to show via API
     */
    public function msg_addError($token, $message)
    {
        $this->_add_msg(self::MSG_ERROR_KEY, $token, $message);
    }

    /**
     * @param $token string     name of group to get it's current status
     * @param bool $asBoolean   return only TRUE|FALSE === OK|ERROR, or code status
     * @return int
     */
    public function getStatus($token = null, $asBoolean = true){

        if(is_null($token)){    # for all tokens

            $tokens = array_flip( array_keys($this->_data['m']) );

            foreach ( $tokens as $token =>& $status_value){
                
                $status_value = $this->_get_status_code_by_token($token);
                
                unset($status_value);

            }
            
            $status = in_array(self::STATUS_ERRROR, $tokens)    # if exists at least one error - whole operation failure 
                ? self::STATUS_ERRROR
                : (in_array(self::STATUS_WARNING, $tokens))     # if exists somewhere warning and no errors - warning
                     ? self::STATUS_WARNING
                     : self::STATUS_OK;                         # no warnings and errors - success 100%


        }else{                  # for current $token

            $status = $this->_get_status_code_by_token( $token );

        }

        #  IS IT OK ONLY    ?  YES|NO                                                      : STATUS CODE
        return $asBoolean   ? (in_array($status, [self::STATUS_WARNING, self::STATUS_OK])) : $status;

    }

    protected function _get_status_code_by_token($token){

        if(array_key_exists($token, $this->_data['m'])){

            $e = count($this->_data['m'][$token][self::MSG_ERROR_KEY]);
            $s = count($this->_data['m'][$token][self::MSG_SUCCESS_KEY]);

        }else{

            $e = $s = 0; // no token - all is ok 100%

        }

        $status =

            ($e > 0 && $s > 0)
                ? self::STATUS_WARNING          # errors + success === success and errors as warnings
                : ($e > 0 && $s == 0)
                    ? self::STATUS_ERRROR       # only errors === error status
                    : self::STATUS_OK;          # only success or no errors === success

        return $status;
    }

    public function toResponse(){

        return new JsonResponse($this->_data);

    }


}